 <script type="text/javascript">
         (function(){
             
               // https://getbootstrap.com/javascript/#carousel
               $('#carousel123').carousel({ interval: 2000 });
               $('#carouselABC').carousel({ interval: 3600 });
             }());
         
         (function(){
           $('.carousel-showmanymoveone .item').each(function(){
             var itemToClone = $(this);
         
             for (var i=1;i<4;i++) {
               itemToClone = itemToClone.next();
         
                   // wrap around if at end of item collection
                   if (!itemToClone.length) {
                     itemToClone = $(this).siblings(':first');
                   }
         
                   // grab item, clone, add marker class, add to collection
                   itemToClone.children(':first-child').clone()
                   .addClass("cloneditem-"+(i))
                   .appendTo($(this));
                 }
               });
         }());
      </script>
      
      <script>
         var flip = 0;
         $( ".adv" ).click(function() {
           $( ".adv-search" ).toggle( flip++ % 2 === 0 );
         });
      </script>
      <script>
         var picker = new Pikaday(
         {
           field: document.getElementById('datepicker'),
           firstDay: 1,
           minDate: new Date(),
           maxDate: new Date(2020, 12, 31),
           yearRange: [2000,2020]
         });
         
      </script>
      <script type="text/javascript">
         $(window).scroll(function()
          {
                                        // 100 = The point you would like to fade the nav in.
          
                                        if ($(window).scrollTop() > 100) {
          
                                          $('.fixed').addClass('is-sticky');
          
                                        } else {
          
                                          $('.fixed').removeClass('is-sticky');
          
                                        };
                                      });
          
      </script>